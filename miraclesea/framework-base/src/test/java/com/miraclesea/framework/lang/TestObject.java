package com.miraclesea.framework.lang;

final class TestObject extends BaseObject {
	
	@SuppressWarnings("unused")
	private String field;
	
	TestObject(final String field) {
		this.field = field;
	}
}
