package com.miraclesea.module.rbac.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.miraclesea.module.rbac.vo.PermissionUtil;
import com.miraclesea.module.rbac.vo.RbacPermission;

public final class RoleTest {
	
	@Test
	public void assignPermissions() {
		Role role = new Role();
		Collection<RbacPermission> permissions = Lists.newArrayList(RbacPermission.values());
		role.assignPermissions(permissions);
		assertThat(role.getAssignedPermissions().size(), is(permissions.size()));
		for (AssignedRolePermission each : role.getAssignedPermissions()) {
			assertThat(each.getRole(), is(role));
			assertTrue(permissions.contains(PermissionUtil.from(each.getPermissionName())));
		}
	}

	@Test
	public void denyPermissions() {
		Role role = new Role();
		Collection<RbacPermission> permissions = Lists.newArrayList(RbacPermission.values());
		role.denyPermissions(permissions);
		assertThat(role.getDeniedPermissions().size(), is(permissions.size()));
		for (DeniedRolePermission each : role.getDeniedPermissions()) {
			assertThat(each.getRole(), is(role));
			assertTrue(permissions.contains(PermissionUtil.from(each.getPermissionName())));
		}
	}
	
	@Test
	public void hasPermission() {
		Role role = new Role();
		role.assignPermissions(Lists.newArrayList(RbacPermission.values()));
		assertTrue(role.hasPermission(RbacPermission.DenyRolePermission));
		role.denyPermissions(Lists.newArrayList(RbacPermission.DenyRolePermission));
		assertFalse(role.hasPermission(RbacPermission.DenyRolePermission));
		assertTrue(role.hasPermission(RbacPermission.AssignRolePermission));
	}
}
