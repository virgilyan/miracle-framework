package com.miraclesea.module.rbac.vo;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public final class PermissionUtilTest {
	
	@Test
	public void fromFailureWithPermissionNameFormatError() {
		try {
			PermissionUtil.from("xxx");
		} catch (final IllegalArgumentException ex) {
			assertThat(ex.getMessage(), is("Permission Name 'xxx' invalid, should be full class name # enum instance. Example: com.miracle.test.TestPermission#Test1"));
		}
	}
	
	@Test
	public void fromFailureWithClassNotFound() {
		try {
			PermissionUtil.from("permission.clazz#EnumValue");
		} catch (final IllegalArgumentException ex) {
			assertThat(ex.getMessage(), is("Not found, permission name is 'permission.clazz#EnumValue', parsed class is 'permission.clazz'"));
		}
	}
	
	@Test
	public void fromFailureWithNotEnum() {
		try {
			PermissionUtil.from("java.lang.Object#AnObject");
		} catch (final IllegalArgumentException ex) {
			assertThat(ex.getMessage(), is("Not a enum, permission name is 'java.lang.Object#AnObject', parsed class is 'java.lang.Object'"));
		}
	}
	
	@Test
	public void fromFailureWithNotPermission() {
		try {
			PermissionUtil.from("com.miraclesea.test.enums.TestEnum#TEST");
		} catch (final IllegalArgumentException ex) {
			assertThat(ex.getMessage(), 
					is("Not a permission, permission name is 'com.miraclesea.test.enums.TestEnum#TEST', parsed class is 'com.miraclesea.test.enums.TestEnum'"));
		}
	}
	
	@Test
	public void fromSuccess() {
		assertThat((TestPermission) PermissionUtil.from("com.miraclesea.module.rbac.vo.TestPermission#TEST"), is(TestPermission.TEST));
	}
	
	@Test
	public void toPermissionString() {
		assertThat(PermissionUtil.toPermissionString(TestPermission.TEST), is("com.miraclesea.module.rbac.vo.TestPermission#TEST"));
	}
}
