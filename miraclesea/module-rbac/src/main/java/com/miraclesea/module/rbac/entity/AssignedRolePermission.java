package com.miraclesea.module.rbac.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("A")
public final class AssignedRolePermission extends RolePermission {
	
	private static final long serialVersionUID = -8457183869355452592L;
	
	@ManyToOne
	@JoinColumn(name = "role_id")
	@JsonIgnore
	private Role role;

}
