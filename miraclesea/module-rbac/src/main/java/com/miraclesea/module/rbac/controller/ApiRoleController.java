package com.miraclesea.module.rbac.controller;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.groups.Default;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.miraclesea.framework.group.New;
import com.miraclesea.module.rbac.entity.Role;
import com.miraclesea.module.rbac.service.RoleService;

@RestController
@RequestMapping("api")
public class ApiRoleController {
	
	@Resource
	private RoleService roleService;
	
	@RequestMapping(value = "roles", method = RequestMethod.GET)
	public Page<Role> findAll() {
		return roleService.findAll(new PageRequest(0, 10));
	}
	
	@RequestMapping(value = "role/{id}", method = RequestMethod.GET)
	public Role load(@PathVariable final String id) {
		return roleService.load(id);
	}
	
	@RequestMapping(value = "role", method = RequestMethod.POST)
	public void create(@RequestBody @Validated({ New.class, Default.class }) final Role role) {
		roleService.create(role);
	}
	
	@RequestMapping(value = "role/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody @Valid final Role role, @PathVariable final String id) {
		role.setId(id);
		roleService.update(role);
	}
	
	@RequestMapping(value = "role/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable final String id) {
		roleService.delete(id);
	}
}
